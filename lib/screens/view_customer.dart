import 'package:flutter/material.dart';
import 'package:web_project/colors/colors.dart';

class ViewCustomers extends StatefulWidget {
  List tableTitle;
  ViewCustomers({this.tableTitle});
  @override
  _ViewCustomersState createState() => _ViewCustomersState();
}

class _ViewCustomersState extends State<ViewCustomers> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: CustomColor.mainColor,
        appBar: AppBar(
          leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(
              Icons.arrow_back,
              color: CustomColor.drawerColor,
              size: 30,
            ),
          ),
          backgroundColor: CustomColor.mainColor,
          title: Text(
            "All Customers",
            style: TextStyle(
                color: CustomColor.drawerColor,
                fontWeight: FontWeight.bold,
                fontSize: 30),
          ),
        ),
        body: Container(
          alignment: Alignment.center,
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    padding: EdgeInsets.all(20),
                    //  height: widget.height,
                    width: MediaQuery.of(context).size.width / 2,
                    decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.white.withOpacity(0.5),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: Offset(0, 3), // changes position of shadow
                          ),
                        ],
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.white,
                        border:
                            Border.all(color: CustomColor.mainColor, width: 2)),
                            
                            // child: ,
                    // child: ListView.builder(
                    //     shrinkWrap: true,
                    //     itemCount: widget.tableTitle.length,
                    //     itemBuilder: (context, index) {
                    //       return Padding(
                    //         padding: const EdgeInsets.all(8),
                    //         child: Column(
                    //           crossAxisAlignment: CrossAxisAlignment.start,
                    //           children: [
                    //             Row(
                    //               children: [
                    //                 Expanded(
                    //                   flex: 1,
                    //                   child: Text(
                    //                     widget.tableTitle[index],
                    //                     style: TextStyle(
                    //                         color: CustomColor.drawerColor,
                    //                         fontWeight: FontWeight.bold,
                    //                         fontSize: 20),
                    //                   ),
                    //                 ),
                    //                 Expanded(
                    //                   flex: 5,
                    //                   child: Padding(
                    //                     padding: const EdgeInsets.all(8.0),
                    //                     child: TextField(
                    //                       decoration: InputDecoration(
                    //                         focusedBorder: OutlineInputBorder(
                    //                           borderSide: BorderSide(
                    //                               color: CustomColor.mainColor),
                    //                         ),
                    //                         enabledBorder: OutlineInputBorder(
                    //                           borderSide: BorderSide(
                    //                               color: CustomColor.mainColor),
                    //                         ),
                    //                         hintText: widget.tableTitle[index],
                    //                         hintStyle: TextStyle(
                    //                             color: CustomColor.drawerColor,
                    //                             fontSize: 15),
                    //                       ),
                    //                     ),
                    //                   ),
                    //                 ),
                    //               ],
                    //             ),
                    //           ],
                    //         ),
                    //       );
                    //     }),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
