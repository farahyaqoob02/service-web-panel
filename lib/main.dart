import 'package:flutter/material.dart';
import 'package:web_project/colors/colors.dart';
import 'package:web_project/desktop.dart';
import 'package:web_project/screens/login.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(canvasColor: CustomColor.drawerColor),
      debugShowCheckedModeBanner: false,
      home: Scaffold(backgroundColor: Colors.cyan, body: Desktop()),
    );
  }
}
