import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Preferences {
  setBodyType(String pageName) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('bodyType', pageName);
  }

  Future<String> getBodyType() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('bodyType');
  }
}
