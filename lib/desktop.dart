import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:web_project/screens/login.dart';

class Desktop extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout(
      desktop: LogIn(
        width: MediaQuery.of(context).size.width / 2.8,
        scale: 9,
      ),
      mobile: LogIn(
        width: MediaQuery.of(context).size.width,
        scale: 15,
      ),
      tablet: LogIn(
        width: MediaQuery.of(context).size.width / 1.5,
        scale: 12,
      ),
    );
  }
}
