import 'package:expansion_card/expansion_card.dart';
import 'package:flutter/material.dart';
import 'package:web_project/colors/colors.dart';
import 'package:web_project/dashboard/about_page.dart';
import 'package:web_project/dashboard/all_customers.dart';
import 'package:web_project/dashboard/all_workers.dart';
import 'package:web_project/dashboard/approved_customers.dart';
import 'package:web_project/dashboard/approved_workers.dart';
import 'package:web_project/dashboard/contact_page.dart';
import 'package:web_project/dashboard/products_page.dart';
import 'package:web_project/dashboard/setting_page.dart';
import 'package:web_project/dashboard/unapproved_customers.dart';
import 'package:web_project/dashboard/unapproved_workers.dart';
import 'package:web_project/extras/expansion1.dart';
import 'package:web_project/extras/expansion2.dart';
import 'package:web_project/extras/shared_preferences.dart';

class MobileDashboard extends StatefulWidget {
  @override
  _MobileDashboardState createState() => _MobileDashboardState();
}

class _MobileDashboardState extends State<MobileDashboard> {
  GlobalKey<ScaffoldState> _drawerKey = GlobalKey();
  String currentPage = "productPage";
  setPageType() async {
    await Preferences().getBodyType().then((value) {
      if (value == "allCustomers") {
        setState(() {
          currentPage = value;
        });
      } else if (value == "approvedCustomers") {
        setState(() {
          currentPage = value;
        });
      } else if (value == "unapprovedCustomers") {
        setState(() {
          currentPage = value;
        });
      } else if (value == "allWorkers") {
        setState(() {
          currentPage = value;
        });
      } else if (value == "approvedWorkers") {
        setState(() {
          currentPage = value;
        });
      } else if (value == "unapprovedWorkers") {
        setState(() {
          currentPage = value;
        });
      } else if (value == "aboutPage") {
        setState(() {
          currentPage = value;
        });
      } else if (value == "contactPage") {
        setState(() {
          currentPage = value;
        });
      } else if (value == "productPage") {
        setState(() {
          currentPage = value;
        });
      } else if (value == "settingPage") {
        setState(() {
          currentPage = value;
        });
      }
    });
  }

  @override
  void initState() {
    setPageType();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: CustomColor.mainColor,
        key: _drawerKey,
        drawer: Drawer(
          child: Container(
            height: MediaQuery.of(context).size.height,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Center(
                        child: Image.asset(
                          "images/logo.png",
                          scale: 25,
                          colorBlendMode: BlendMode.clear,
                        ),
                      ),
                      Center(
                        child: Text(
                          "Admin Panel",
                          style: TextStyle(
                              color: CustomColor.mainColor,
                              fontSize: 20,
                              fontWeight: FontWeight.w600),
                        ),
                      ),
                      Divider(
                        thickness: 1,
                      ),
                      CustomExpansionCard1(
                        icon: Icons.info,
                        text: "Products",
                        childText1: "Dashboard1",
                        childText2: "Dashboard2",
                        onPressed1: () {
                          Preferences().setBodyType("productPage");
                          setState(() {
                            currentPage = "productPage";
                            if (_drawerKey.currentState.isDrawerOpen) {
                              Navigator.pop(context);
                            }
                          });
                        },
                        onPressed2: () {
                          Preferences().setBodyType("productPage");
                          setState(() {
                            currentPage = "productPage";
                            if (_drawerKey.currentState.isDrawerOpen) {
                              Navigator.pop(context);
                            }
                          });
                        },
                      ),
                      CustomExpansionCard2(
                        icon: Icons.person,
                        text: "Customers",
                        childText1: "All",
                        childText2: "Approved",
                        childText3: "Non Approved",
                        onPressed1: () {
                          Preferences().setBodyType("allCustomers");
                          setState(() {
                            currentPage = "allCustomers";
                            if (_drawerKey.currentState.isDrawerOpen) {
                              Navigator.pop(context);
                            }
                          });
                        },
                        onPressed2: () {
                          Preferences().setBodyType("approvedCustomers");
                          setState(() {
                            currentPage = "approvedCustomers";
                            if (_drawerKey.currentState.isDrawerOpen) {
                              Navigator.pop(context);
                            }
                          });
                        },
                        onPressed3: () {
                          Preferences().setBodyType("unapprovedCustomers");
                          setState(() {
                            currentPage = "unapprovedCustomers";
                            if (_drawerKey.currentState.isDrawerOpen) {
                              Navigator.pop(context);
                            }
                          });
                        },
                      ),
                      CustomExpansionCard2(
                        icon: Icons.work_rounded,
                        text: "Workers",
                        childText1: "All",
                        childText2: "Approved",
                        childText3: "Non Approved",
                        onPressed1: () {
                          Preferences().setBodyType("allWorkers");
                          setState(() {
                            currentPage = "allWorkers";
                            if (_drawerKey.currentState.isDrawerOpen) {
                              Navigator.pop(context);
                            }
                          });
                        },
                        onPressed2: () {
                          Preferences().setBodyType("approvedWorkers");
                          setState(() {
                            currentPage = "approvedWorkers";
                            if (_drawerKey.currentState.isDrawerOpen) {
                              Navigator.pop(context);
                            }
                          });
                        },
                        onPressed3: () {
                          Preferences().setBodyType("unapprovedWorkers");
                          setState(() {
                            currentPage = "unapprovedWorkers";
                            if (_drawerKey.currentState.isDrawerOpen) {
                              Navigator.pop(context);
                            }
                          });
                        },
                      ),
                      CustomExpansionCard1(
                        icon: Icons.settings,
                        text: "Settings",
                        childText1: "Dashboard1",
                        childText2: "Dashboard2",
                        onPressed1: () {
                          Preferences().setBodyType("settingPage");

                          setState(() {
                            currentPage = "settingPage";
                            if (_drawerKey.currentState.isDrawerOpen) {
                              Navigator.pop(context);
                            }
                          });
                        },
                        onPressed2: () {
                          Preferences().setBodyType("settingPage");
                          setState(() {
                            currentPage = "settingPage";
                            if (_drawerKey.currentState.isDrawerOpen) {
                              Navigator.pop(context);
                            }
                          });
                        },
                      ),
                      // Padding(
                      //   padding: const EdgeInsets.only(top: 8.0, left: 15),
                      //   child: Row(
                      //     children: [
                      //       Icon(
                      //         Icons.info_rounded,
                      //         color: CustomColor.mainColor,
                      //         size: 22,
                      //       ),
                      //       SizedBox(
                      //         width: 5,
                      //       ),
                      //       GestureDetector(
                      //         onTap: () {
                      //           setState(() {
                      //             users = false;
                      //             setting = false;
                      //             about = true;
                      //             contact = false;
                      //             if (_drawerKey.currentState.isDrawerOpen) {
                      //               Navigator.pop(context);
                      //             }
                      //           });
                      //         },
                      //         child: Text(
                      //           "About Us",
                      //           style: TextStyle(
                      //               color: CustomColor.mainColor, fontSize: 17),
                      //         ),
                      //       ),
                      //     ],
                      //   ),
                      // ),
                      ExpansionCard(
                        trailing: Icon(
                          Icons.arrow_back,
                          color: CustomColor.drawerColor,
                        ),
                        margin: EdgeInsets.zero,
                        title: Row(
                          children: [
                            Icon(
                              Icons.info_rounded,
                              color: CustomColor.mainColor,
                              size: 17,
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            GestureDetector(
                              onTap: () {
                                Preferences().setBodyType("aboutPage");
                                setState(() {
                                  currentPage = "aboutPage";
                                  if (_drawerKey.currentState.isDrawerOpen) {
                                    Navigator.pop(context);
                                  }
                                });
                              },
                              child: Text(
                                "About Us",
                                style: TextStyle(
                                    color: CustomColor.mainColor, fontSize: 15),
                              ),
                            ),
                          ],
                        ),
                      ),
                      // Padding(
                      //   padding: const EdgeInsets.only(top: 20, left: 15),
                      //   child: Row(
                      //     children: [
                      //       Icon(
                      //         Icons.call,
                      //         color: CustomColor.mainColor,
                      //         size: 22,
                      //       ),
                      //       SizedBox(
                      //         width: 5,
                      //       ),
                      //       GestureDetector(
                      //         onTap: () {
                      //           setState(() {
                      //             users = false;
                      //             setting = false;
                      //             about = false;
                      //             contact = true;
                      //             if (_drawerKey.currentState.isDrawerOpen) {
                      //               Navigator.pop(context);
                      //             }
                      //           });
                      //         },
                      //         child: Text(
                      //           "Contact Us",
                      //           style: TextStyle(
                      //               color: CustomColor.mainColor, fontSize: 17),
                      //         ),
                      //       ),
                      //     ],
                      //   ),
                      // ),
                      ExpansionCard(
                        trailing: Icon(
                          Icons.arrow_back,
                          color: CustomColor.drawerColor,
                        ),
                        margin: EdgeInsets.zero,
                        title: Row(
                          children: [
                            Icon(
                              Icons.call,
                              color: CustomColor.mainColor,
                              size: 17,
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            GestureDetector(
                              onTap: () {
                                Preferences().setBodyType("contactPage");
                                setState(() {
                                  currentPage = "contactPage";
                                  if (_drawerKey.currentState.isDrawerOpen) {
                                    Navigator.pop(context);
                                  }
                                });
                              },
                              child: Text(
                                "Contact Us",
                                style: TextStyle(
                                    color: CustomColor.mainColor, fontSize: 15),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: [
                        Divider(
                          thickness: 1,
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Icon(
                              Icons.logout,
                              color: CustomColor.mainColor,
                              size: 17,
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              "Logout",
                              style: TextStyle(
                                  color: CustomColor.mainColor, fontSize: 15),
                            ),
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
        body: ListView(
          children: [
            Container(
              alignment: Alignment.topLeft,
              color: CustomColor.mainColor,
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: currentPage == "allCustomers"
                  ? AllCustomers(
                    value: false,
                    width:1,
                      icon: GestureDetector(
                        onTap: () {
                          _drawerKey.currentState.openDrawer();
                        },
                        child: Icon(
                          Icons.menu,
                          color: CustomColor.drawerColor,
                          size: 25,
                        ),
                      ),
                    )
                  : currentPage == "settingPage"
                      ? Setting(
                          icon: GestureDetector(
                            onTap: () {
                              _drawerKey.currentState.openDrawer();
                            },
                            child: Icon(
                              Icons.menu,
                              color: CustomColor.drawerColor,
                              size: 25,
                            ),
                          ),
                        )
                      : currentPage == "aboutPage"
                          ? AboutPage(
                              icon: GestureDetector(
                                onTap: () {
                                  _drawerKey.currentState.openDrawer();
                                },
                                child: Icon(
                                  Icons.menu,
                                  color: CustomColor.drawerColor,
                                  size: 25,
                                ),
                              ),
                            )
                          : currentPage == "contactPage"
                              ? ContactPage(
                                  icon: GestureDetector(
                                    onTap: () {
                                      _drawerKey.currentState.openDrawer();
                                    },
                                    child: Icon(
                                      Icons.menu,
                                      color: CustomColor.drawerColor,
                                      size: 25,
                                    ),
                                  ),
                                )
                              : currentPage == "productPage"
                                  ? ProductsPage(
                                      icon: GestureDetector(
                                        onTap: () {
                                          _drawerKey.currentState.openDrawer();
                                        },
                                        child: Icon(
                                          Icons.menu,
                                          color: CustomColor.drawerColor,
                                          size: 25,
                                        ),
                                      ),
                                    )
                                  : currentPage == "approvedCustomers"
                                      ? ApprovedCustomers(
                                          icon: GestureDetector(
                                            onTap: () {
                                              _drawerKey.currentState
                                                  .openDrawer();
                                            },
                                            child: Icon(
                                              Icons.menu,
                                              color: CustomColor.drawerColor,
                                              size: 25,
                                            ),
                                          ),
                                        )
                                      : currentPage == "unapprovedCustomers"
                                          ? UnapprovedCustomers(
                                              icon: GestureDetector(
                                                onTap: () {
                                                  _drawerKey.currentState
                                                      .openDrawer();
                                                },
                                                child: Icon(
                                                  Icons.menu,
                                                  color:
                                                      CustomColor.drawerColor,
                                                  size: 25,
                                                ),
                                              ),
                                            )
                                          : currentPage == "allWorkers"
                                              ? AllWorkers(
                                                  icon: GestureDetector(
                                                    onTap: () {
                                                      _drawerKey.currentState
                                                          .openDrawer();
                                                    },
                                                    child: Icon(
                                                      Icons.menu,
                                                      color: CustomColor
                                                          .drawerColor,
                                                      size: 25,
                                                    ),
                                                  ),
                                                )
                                              : currentPage == "approvedWorkers"
                                                  ? ApprovedWorkers(
                                                      icon: GestureDetector(
                                                        onTap: () {
                                                          _drawerKey
                                                              .currentState
                                                              .openDrawer();
                                                        },
                                                        child: Icon(
                                                          Icons.menu,
                                                          color: CustomColor
                                                              .drawerColor,
                                                          size: 25,
                                                        ),
                                                      ),
                                                    )
                                                  : currentPage ==
                                                          "unapprovedWorkers"
                                                      ? UnapprovedWorkers(
                                                          icon: GestureDetector(
                                                            onTap: () {
                                                              _drawerKey
                                                                  .currentState
                                                                  .openDrawer();
                                                            },
                                                            child: Icon(
                                                              Icons.menu,
                                                              color: CustomColor
                                                                  .drawerColor,
                                                              size: 25,
                                                            ),
                                                          ),
                                                        )
                                                      : ProductsPage(
                                                          icon: GestureDetector(
                                                            onTap: () {
                                                              _drawerKey
                                                                  .currentState
                                                                  .openDrawer();
                                                            },
                                                            child: Icon(
                                                              Icons.menu,
                                                              color: CustomColor
                                                                  .drawerColor,
                                                              size: 25,
                                                            ),
                                                          ),
                                                        ),
            ),
          ],
        ),
      ),
    );
  }
}
