import 'package:expansion_card/expansion_card.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:web_project/colors/colors.dart';
import 'package:web_project/dashboard/about_page.dart';
import 'package:web_project/dashboard/all_customers.dart';
import 'package:web_project/dashboard/all_workers.dart';
import 'package:web_project/dashboard/approved_customers.dart';
import 'package:web_project/dashboard/approved_workers.dart';
import 'package:web_project/dashboard/contact_page.dart';
import 'package:web_project/dashboard/products_page.dart';
import 'package:web_project/dashboard/unapproved_customers.dart';
import 'package:web_project/dashboard/setting_page.dart';
import 'package:web_project/dashboard/unapproved_workers.dart';
import 'package:web_project/extras/expansion1.dart';
import 'package:web_project/extras/expansion2.dart';
import 'package:web_project/extras/shared_preferences.dart';

class DesktopDashboard extends StatefulWidget {
  double dashboardWidth;
  double drawerWidth;
  DesktopDashboard({this.dashboardWidth, this.drawerWidth});
  @override
  _DesktopDashboardState createState() => _DesktopDashboardState();
}

class _DesktopDashboardState extends State<DesktopDashboard> {
  String currentPage = "productPage";
  setPageType() async {
    await Preferences().getBodyType().then((value) {
      if (value == "allCustomers") {
        setState(() {
          currentPage = value;
        });
      } else if (value == "approvedCustomers") {
        setState(() {
          currentPage = value;
        });
      } else if (value == "unapprovedCustomers") {
        setState(() {
          currentPage = value;
        });
      } else if (value == "allWorkers") {
        setState(() {
          currentPage = value;
        });
      } else if (value == "approvedWorkers") {
        setState(() {
          currentPage = value;
        });
      } else if (value == "unapprovedWorkers") {
        setState(() {
          currentPage = value;
        });
      } else if (value == "aboutPage") {
        setState(() {
          currentPage = value;
        });
      } else if (value == "contactPage") {
        setState(() {
          currentPage = value;
        });
      } else if (value == "productPage") {
        setState(() {
          currentPage = value;
        });
      } else if (value == "settingPage") {
        setState(() {
          currentPage = value;
        });
      }
    });
  }

  @override
  void initState() {
    setPageType();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        shrinkWrap: true,
        children: [
          Container(
              child: Row(
            children: [
              Column(
                children: [
                  Container(
                    height: MediaQuery.of(context).size.height,
                    width:
                        MediaQuery.of(context).size.width * widget.drawerWidth,
                    color: CustomColor.drawerColor,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Center(
                                child: Image.asset(
                                  "images/logo.png",
                                  scale: 20,
                                  colorBlendMode: BlendMode.clear,
                                ),
                              ),
                              SizedBox(
                                width: 7,
                              ),
                              Center(
                                child: Text(
                                  "Admin Panel",
                                  style: TextStyle(
                                      color: CustomColor.mainColor,
                                      fontSize: 21,
                                      fontWeight: FontWeight.w600),
                                ),
                              ),
                              Divider(
                                thickness: 1,
                              ),
                              CustomExpansionCard1(
                                icon: Icons.info,
                                text: "Products",
                                childText1: "Dashboard1",
                                childText2: "Dashboard2",
                                onPressed1: () {
                                  Preferences().setBodyType("productPage");
                                  setState(() {
                                    currentPage = "productPage";
                                  });
                                },
                                onPressed2: () {
                                  Preferences().setBodyType("productPage");
                                  setState(() {
                                    currentPage = "productPage";
                                  });
                                },
                              ),
                              CustomExpansionCard2(
                                icon: Icons.person,
                                text: "Customers",
                                childText1: "All",
                                childText2: "Approved",
                                childText3: "Non Approved",
                                onPressed1: () {
                                  Preferences().setBodyType("allCustomers");
                                  setState(() {
                                    currentPage = "allCustomers";
                                  });
                                },
                                onPressed2: () {
                                  Preferences()
                                      .setBodyType("approvedCustomers");
                                  setState(() {
                                    currentPage = "approvedCustomers";
                                  });
                                },
                                onPressed3: () {
                                  Preferences()
                                      .setBodyType("unapprovedCustomers");
                                  setState(() {
                                    currentPage = "unapprovedCustomers";
                                  });
                                },
                              ),
                              CustomExpansionCard2(
                                icon: Icons.work_rounded,
                                text: "Workers",
                                childText1: "All",
                                childText2: "Approved",
                                childText3: "Non Approved",
                                onPressed1: () {
                                  Preferences().setBodyType("allWorkers");
                                  setState(() {
                                    currentPage = "allWorkers";
                                  });
                                },
                                onPressed2: () {
                                  Preferences().setBodyType("approvedWorkers");
                                  setState(() {
                                    currentPage = "approvedWorkers";
                                  });
                                },
                                onPressed3: () {
                                  Preferences()
                                      .setBodyType("unapprovedWorkers");
                                  setState(() {
                                    currentPage = "unapprovedWorkers";
                                  });
                                },
                              ),
                              CustomExpansionCard1(
                                icon: Icons.settings,
                                text: "Settings",
                                childText1: "Dashboard1",
                                childText2: "Dashboard2",
                                onPressed1: () {
                                  Preferences().setBodyType("settingPage");
                                  setState(() {
                                    currentPage = "settingPage";
                                  });
                                },
                                onPressed2: () {
                                  Preferences().setBodyType("settingPage");
                                  setState(() {
                                    currentPage = "settingPage";
                                  });
                                },
                              ),
                              ExpansionCard(
                                trailing: Icon(
                                  Icons.arrow_back,
                                  color: CustomColor.drawerColor,
                                ),
                                margin: EdgeInsets.zero,
                                title: Row(
                                  children: [
                                    Icon(
                                      Icons.info_rounded,
                                      color: CustomColor.mainColor,
                                      size: 17,
                                    ),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        Preferences().setBodyType("aboutPage");
                                        setState(() {
                                          currentPage = "aboutPage";
                                        });
                                      },
                                      child: Text(
                                        "About Us",
                                        style: TextStyle(
                                            color: CustomColor.mainColor,
                                            fontSize: 15),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              ExpansionCard(
                                trailing: Icon(
                                  Icons.arrow_back,
                                  color: CustomColor.drawerColor,
                                ),
                                margin: EdgeInsets.zero,
                                title: Row(
                                  children: [
                                    Icon(
                                      Icons.call,
                                      color: CustomColor.mainColor,
                                      size: 17,
                                    ),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        Preferences()
                                            .setBodyType("contactPage");
                                        setState(() {
                                          currentPage = "contactPage";
                                        });
                                      },
                                      child: Text(
                                        "Contact Us",
                                        style: TextStyle(
                                            color: CustomColor.mainColor,
                                            fontSize: 15),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              children: [
                                Divider(
                                  thickness: 1,
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Icon(
                                      Icons.logout,
                                      color: CustomColor.mainColor,
                                      size: 17,
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Text(
                                      "Logout",
                                      style: TextStyle(
                                          color: CustomColor.mainColor,
                                          fontSize: 15),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Column(
                children: [
                  Container(
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width *
                          widget.dashboardWidth,
                      color: CustomColor.mainColor,
                      child: currentPage == "allCustomers"
                          ? AllCustomers(
                              value: true,
                              icon: null,
                              width: widget.dashboardWidth,
                            )
                          : currentPage == "settingPage"
                              ? Setting(
                                  icon: null,
                                )
                              : currentPage == "aboutPage"
                                  ? AboutPage(
                                      icon: null,
                                    )
                                  : currentPage == "contactPage"
                                      ? ContactPage(
                                          icon: null,
                                        )
                                      : currentPage == "productPage"
                                          ? ProductsPage(
                                              icon: null,
                                            )
                                          : currentPage == "approvedCustomers"
                                              ? ApprovedCustomers(
                                                  icon: null,
                                                )
                                              : currentPage ==
                                                      "unapprovedCustomers"
                                                  ? UnapprovedCustomers(
                                                      icon: null,
                                                    )
                                                  : currentPage == "allWorkers"
                                                      ? AllWorkers(
                                                          icon: null,
                                                        )
                                                      : currentPage ==
                                                              "approvedWorkers"
                                                          ? ApprovedWorkers(
                                                              icon: null,
                                                            )
                                                          : currentPage ==
                                                                  "unapprovedWorkers"
                                                              ? UnapprovedWorkers(
                                                                  icon: null,
                                                                )
                                                              : ProductsPage(
                                                                  icon: null,
                                                                )),
                ],
              )
            ],
          ))
        ],
      ),
    );
  }
}
