import 'package:flutter/material.dart';
import 'package:web_project/colors/colors.dart';

class ApprovedCustomers extends StatefulWidget {
   final Widget  icon;
  ApprovedCustomers({this.icon});
  @override
  _ApprovedCustomersState createState() => _ApprovedCustomersState();
}

class _ApprovedCustomersState extends State<ApprovedCustomers> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
         backgroundColor: CustomColor.mainColor,
      appBar: widget.icon==null?AppBar(
        
        title: Text(
          "Approved Customers",
          style: TextStyle(
              color: CustomColor.drawerColor,
              fontWeight: FontWeight.bold,
              fontSize: 30),
        ),
        backgroundColor: CustomColor.mainColor,
        
      ):AppBar(
         leading: widget.icon,
          backgroundColor: CustomColor.mainColor,
        title: Text(
          "Approved Customers",
          style: TextStyle(
              color: CustomColor.drawerColor,
              fontWeight: FontWeight.bold,
              fontSize: 30),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // Text(
                //   "Approved Customers",
                //   style: TextStyle(
                //       color: CustomColor.drawerColor,
                //       fontWeight: FontWeight.bold,
                //       fontSize: 30),
                // ),
                // Divider(
                //   thickness: 2,
                //   color: CustomColor.textColor,
                // )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
