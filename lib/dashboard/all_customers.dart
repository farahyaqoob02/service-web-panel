import 'package:bidirectional_scroll_view/bidirectional_scroll_view.dart';
import 'package:flutter/material.dart';
import 'package:web_project/colors/colors.dart';
import 'package:web_project/screens/view_customer.dart';

class AllCustomers extends StatefulWidget {
  final Widget icon;
  final double width;
  final bool value;
  AllCustomers({this.icon, this.width, this.value});
  @override
  _AllCustomersState createState() => _AllCustomersState();
}

class _AllCustomersState extends State<AllCustomers> {
  List<Map> customer = [
    {
      "id": "1",
      "name": "Farah",
      "number": "0649393773",
      "details": "xyz customer",
      "address": "Shah Faisal",
      "information": "yes",
      "job": "Civil engineer",
      "action": "buttons",
      "salary": "1000",
      "lastName": "Yaqoob",
    },
    {
      "id": "1",
      "name": "Farah",
      "number": "0649393773",
      "details": "xyz customer",
      "address": "Shah Faisal",
      "information": "yes",
      "job": "Civil engineer",
      "action": "buttons",
      "salary": "1000",
      "lastName": "Yaqoob",
    },
    {
      "id": "1",
      "name": "Farah",
      "number": "0649393773",
      "details": "xyz customer",
      "address": "Shah Faisal",
      "information": "yes",
      "job": "Civil engineer",
      "action": "buttons",
      "salary": "1000",
      "lastName": "Yaqoob",
    },
    {
      "id": "1",
      "name": "Farah",
      "number": "0649393773",
      "details": "xyz customer",
      "address": "Shah Faisal",
      "information": "yes",
      "job": "Civil engineer",
      "action": "buttons",
      "salary": "1000",
      "lastName": "Yaqoob",
    },
    {
      "id": "1",
      "name": "Farah",
      "number": "0649393773",
      "details": "xyz customer",
      "address": "Shah Faisal",
      "information": "yes",
      "job": "Civil engineer",
      "action": "buttons",
      "salary": "1000",
      "lastName": "Yaqoob",
    },
    {
      "id": "1",
      "name": "Farah",
      "number": "0649393773",
      "details": "xyz customer",
      "address": "Shah Faisal",
      "information": "yes",
      "job": "Civil engineer",
      "action": "buttons",
      "salary": "1000",
      "lastName": "Yaqoob",
    },
    {
      "id": "1",
      "name": "Farah",
      "number": "0649393773",
      "details": "xyz customer",
      "address": "Shah Faisal",
      "information": "yes",
      "job": "Civil engineer",
      "action": "buttons",
      "salary": "1000",
      "lastName": "Yaqoob",
    },
    {
      "id": "1",
      "name": "Farah",
      "number": "0649393773",
      "details": "xyz customer",
      "address": "Shah Faisal",
      "information": "yes",
      "job": "Civil engineer",
      "action": "buttons",
      "salary": "1000",
      "lastName": "Yaqoob",
    },
    {
      "id": "1",
      "name": "Farah",
      "number": "0649393773",
      "details": "xyz customer",
      "address": "Shah Faisal",
      "information": "yes",
      "job": "Civil engineer",
      "action": "buttons",
      "salary": "1000",
      "lastName": "Yaqoob",
    },
    {
      "id": "1",
      "name": "Farah",
      "number": "0649393773",
      "details": "xyz customer",
      "address": "Shah Faisal",
      "information": "yes",
      "job": "Civil engineer",
      "action": "buttons",
      "salary": "1000",
      "lastName": "Yaqoob",
    },
  ];
  List tableTitle = [
    "Customer Id",
    "Name",
    "Number",
    "Details",
    "Address",
    "Information",
    "Job",
    "salary",
    "lastName",
    "Actions",
  ];
  TableCell getTableTitle(int index) {
    return TableCell(
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 8.0),
        child: Text(
          tableTitle[index],
          style: TextStyle(
              color: CustomColor.drawerColor,
              fontWeight: FontWeight.bold,
              fontSize: 17),
        ),
      ),
    );
  }

  TableRow getTableChildren(int index) {
    return TableRow(children: [
      TableCell(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 8.0),
          child: Padding(
            padding: const EdgeInsets.only(left: 10),
            child: Text(
              customer[index]["id"],
              style: TextStyle(fontSize: 15),
            ),
          ),
        ),
      ),
      TableCell(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 8.0),
          child: Text(
            customer[index]["name"],
            style: TextStyle(fontSize: 15),
          ),
        ),
      ),
      TableCell(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 8.0),
          child: Text(
            customer[index]["number"],
            style: TextStyle(fontSize: 15),
          ),
        ),
      ),
      TableCell(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 8.0),
          child: Text(
            customer[index]["details"],
            style: TextStyle(fontSize: 15),
          ),
        ),
      ),
      TableCell(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 8.0),
          child: Text(
            customer[index]["address"],
            style: TextStyle(fontSize: 15),
          ),
        ),
      ),
      TableCell(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 8.0),
          child: Text(
            customer[index]["information"],
            style: TextStyle(fontSize: 15),
          ),
        ),
      ),
      TableCell(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 8.0),
          child: Text(
            customer[index]["job"],
            style: TextStyle(fontSize: 15),
          ),
        ),
      ),
      TableCell(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 8.0),
          child: Text(
            customer[index]["salary"],
            style: TextStyle(fontSize: 15),
          ),
        ),
      ),
      TableCell(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 8.0),
          child: Text(
            customer[index]["lastName"],
            style: TextStyle(fontSize: 15),
          ),
        ),
      ),
      TableCell(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 8.0),
          child: Row(
            children: [
              IconButton(
                  icon: Icon(
                    Icons.edit,
                    color: Colors.green[400],
                  ),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ViewCustomers(
                                  tableTitle: tableTitle,
                                )));
                  }),
              IconButton(
                  icon: Icon(
                    Icons.delete,
                    color: Colors.red,
                  ),
                  onPressed: () {}),
              IconButton(
                  icon: Icon(
                    Icons.visibility,
                    color: CustomColor.drawerColor,
                  ),
                  onPressed: () {})
            ],
          ),
        ),
      ),
    ]);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: CustomColor.mainColor,
        appBar: widget.icon == null
            ? AppBar(
                title: Text(
                  "All Customers",
                  style: TextStyle(
                      color: CustomColor.drawerColor,
                      fontWeight: FontWeight.bold,
                      fontSize: 30),
                ),
                backgroundColor: CustomColor.mainColor,
              )
            : AppBar(
                leading: widget.icon,
                backgroundColor: CustomColor.mainColor,
                title: Text(
                  "All Customers",
                  style: TextStyle(
                      color: CustomColor.drawerColor,
                      fontWeight: FontWeight.bold,
                      fontSize: 30),
                ),
              ),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Padding(
            padding: const EdgeInsets.all(25),
            child: BidirectionalScrollViewPlugin(
              scrollOverflow: Overflow.clip,
              child: Container(
                width: widget.value ? 1800 * widget.width : 1300,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10)),
                child: Padding(
                  padding: const EdgeInsets.all(15),
                  child: Table(
                    border: TableBorder(
                        horizontalInside: BorderSide(
                            width: 1,
                            color: CustomColor.mainColor,
                            style: BorderStyle.solid)),
                    children: [
                          TableRow(
                            decoration: BoxDecoration(
                              border: Border(bottom: BorderSide(width: 2,color: CustomColor.textColor))
                            ),
                              children: tableTitle.map((e) {
                            var index = tableTitle.indexOf(e);
                            return getTableTitle(index);
                          }).toList())
                        ] +
                        customer.map((element) {
                          // get index
                          var index = customer.indexOf(element);
                          return getTableChildren(index);
                        }).toList(),
                  ),
                ),
              ),
            ),
          ),
        ));
  }
}
