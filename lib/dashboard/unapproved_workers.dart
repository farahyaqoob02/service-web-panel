import 'package:flutter/material.dart';
import 'package:web_project/colors/colors.dart';

class UnapprovedWorkers extends StatefulWidget {
  final Widget icon;
  UnapprovedWorkers({this.icon});
  @override
  _UnapprovedWorkersState createState() => _UnapprovedWorkersState();
}

class _UnapprovedWorkersState extends State<UnapprovedWorkers> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColor.mainColor,
      appBar: widget.icon == null
          ? AppBar(
              title: Text(
                "Unapproved Workers",
                style: TextStyle(
                    color: CustomColor.drawerColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 30),
              ),
              backgroundColor: CustomColor.mainColor,
            )
          : AppBar(
              leading: widget.icon,
              backgroundColor: CustomColor.mainColor,
              title: Text(
                "Unapproved Workers",
                style: TextStyle(
                    color: CustomColor.drawerColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 30),
              ),
            ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // Text(
                //   "Unapproved Workers",
                //   style: TextStyle(
                //       color: CustomColor.drawerColor,
                //       fontWeight: FontWeight.bold,
                //       fontSize: 30),
                // ),
                // Divider(
                //   thickness: 2,
                //   color: CustomColor.textColor,
                // )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
