import 'package:flutter/material.dart';
import 'package:web_project/colors/colors.dart';

class UnapprovedCustomers extends StatefulWidget {
   final Widget  icon;
  UnapprovedCustomers({this.icon});
  @override
  _UnapprovedCustomersState createState() => _UnapprovedCustomersState();
}

class _UnapprovedCustomersState extends State<UnapprovedCustomers> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
         backgroundColor: CustomColor.mainColor,
      appBar: widget.icon==null?AppBar(
        
        title: Text(
          "Unapproved Customers",
          style: TextStyle(
              color: CustomColor.drawerColor,
              fontWeight: FontWeight.bold,
              fontSize: 30),
        ),
        backgroundColor: CustomColor.mainColor,
        
      ):AppBar(
         leading: widget.icon,
          backgroundColor: CustomColor.mainColor,
        title: Text(
                  "Unapproved Customers",
                  style: TextStyle(
                      color: CustomColor.drawerColor,
                      fontWeight: FontWeight.bold,
                      fontSize: 30),
                ),
      ),
          body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView(
                                          
          children: [
            Column(crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // Text(
                //   "Unapproved Customers",
                //   style: TextStyle(
                //       color: CustomColor.drawerColor,
                //       fontWeight: FontWeight.bold,
                //       fontSize: 30),
                // ),
                // Divider(
                //   thickness: 2,
                //   color: CustomColor.textColor,
                // )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
