import 'package:flutter/material.dart';
import 'package:web_project/colors/colors.dart';

class AboutPage extends StatefulWidget {
  final Widget icon;
  AboutPage({this.icon});
  @override
  _AboutPageState createState() => _AboutPageState();
}

class _AboutPageState extends State<AboutPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColor.mainColor,
      appBar: widget.icon==null?AppBar(
        
        title: Text(
          "About",
          style: TextStyle(
              color: CustomColor.drawerColor,
              fontWeight: FontWeight.bold,
              fontSize: 30),
        ),
        backgroundColor: CustomColor.mainColor,
        
      ):AppBar(
        backgroundColor: CustomColor.mainColor,
        leading:widget.icon,
        title: Text(
          "About",
          style: TextStyle(
              color: CustomColor.drawerColor,
              fontWeight: FontWeight.bold,
              fontSize: 30),
        ),

      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // Text(
                //   "About",
                //   style: TextStyle(
                //       color: CustomColor.drawerColor,
                //       fontWeight: FontWeight.bold,
                //       fontSize: 30),
                // ),
                // Divider(
                //   thickness: 2,
                //   color: CustomColor.textColor,
                // )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
